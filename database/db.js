const mysql = require('mysql');

const pool = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
});

pool.getConnection((err, connection)=>{
    if(err){
        console.error('Conexion con la base de datos perdida');
    }
    if(connection){
        connection.release();
        console.log('Conexion con la base de datos exitosa');
        return;
    }
});

module.exports = pool;
const controller = {};



controller.listUsers = (req, res)=>
{
    req.getConnection ((err,conn)=>
    {
        conn.query('SELECT * from usuarios',(err, users)=>{
            if(err){
                res.json(err);
            }
            res.render('users',{
                data:users
            });
        });
    });
};
controller.saveUser = (req, res)=>
{
    const nombre = req.body.nombre;
    const email = req.body.correoE;
    const contraseña = req.body.contra;
    let confirmar = req.body.confirmar;
    const rol = req.body.rol;
    //contador para contar los errores
    let countErr = 0;
    console.log(nombre+email+contraseña+rol);
    req.getConnection((err, conn)=>
    {
        //verificamos que los campos no esten vacios
        if(!nombre || !email || !contraseña || !confirmar || !rol){
            //si entro aqui sumara 1 error
            countErr++; 
            req.flash('error_msg','\n'+countErr+': Campos vacios');              
        }
        //verificamos que las contraseñas coincidan
        if(contraseña != confirmar){
             //si entro aqui sumara 1 error
            countErr++; 
            req.flash('error_msg','\n'+countErr+': Las contraseñas no coinciden');       
        }
        //verificamos que la contraseña no sea muy corta
        if(contraseña.length<5){
            //si entro aqui sumara 1 error
            countErr++; 
            req.flash('error_msg','\n'+countErr+': La contraseña es muy corta, minimo 5 caracteres');         
        }
        //verificamos que el correo que el usuario ingrese no exista ya
        conn.query("select correoE from usuarios where correoE = ?",[email],(err, user)=>{         
            if(user[0]){      
                //Si entro aqui llevamos otro error, sumara 1 o 2 errores              
                countErr++;    
                req.flash('error_msg','\n'+countErr+': El correo electronico ya existe');                        
            }
            //si entro en un error, el contador sumara 1, por lo tanto, no entra a registrar el usuario
            //si entra en los dos, el contador sumara 2, por lo tanto, no entra a registrar el usuario
            //Si no entro a ninguno, el contador seguira valiendo 0, por lo tanto, se procede a registrar el usuario
            if(countErr==0){
                conn.query('insert into usuarios VALUES (null, ?,?,?,?)',[nombre,email,contraseña,rol]);
                req.flash('success_msg','Usuario registrado con exito'); 
            }   
            //ponemos el redirect al final, ya que, primero debemos establecer las respuestas y luego enviarselas al cliente
            //(req.flash funciona como una respuesta para el cliente)
                                                      
            res.redirect('/users');      
        })
    });
};

controller.editUser = (req, res)=>{
    const userid = req.params.idusuario;
    req.getConnection((err, conn)=>{
        conn.query('select * from usuarios where idusuario = ?',[userid], (err, users)=>{
            res.render('users_edit',{
                data:users[0]
            });
        });
    });
};
controller.search = (req, res)=>{
    const buscar = req.body.buscar;
    console.log(buscar)
    req.getConnection((err, conn)=>{
        conn.query("select * from usuarios where nombre like ?",['%'+buscar+'%'],(err, user)=>{
            console.log(user);
            
            res.render('users',{
                
                data:user
                
            })
        })
    })
}
controller.updateUser=(req, res)=>{
    const userid = req.params.idusuario;
    const nombre = req.body.nombre;
    const email = req.body.correoE;
    const contraseña = req.body.contra;
    let confirmar = req.body.confirmar;
    const rol = req.body.rol;
    //contador para contar los errores
    let updateErr = 0;
    console.log(nombre+email+contraseña+rol);

    req.getConnection((err, conn)=>{
        //verificamos que los campos no esten vacios
        if(!nombre || !email || !contraseña || !confirmar || !rol){
            //si entro aqui sumara 1 error
            updateErr++; 
            req.flash('error_msg','\n'+updateErr+': Campos vacios');  
            res.redirect('/updateuser/'+userid);  
                   
        }
        //verificamos que las contraseñas coincidan
        if(contraseña != confirmar){
             //si entro aqui sumara 1 error
             updateErr++; 
            req.flash('error_msg','\n'+updateErr+': Las contraseñas no coinciden');  
            res.redirect('/updateuser/'+userid);  
              
        }
        //verificamos que la contraseña no sea muy corta
        if(contraseña.length<5){
            //si entro aqui sumara 1 error
            updateErr++; 
            req.flash('error_msg','\n'+updateErr+': La contraseña es muy corta, minimo 5 caracteres');   
            res.redirect('/updateuser/'+userid);   
              
        }
        //verificamos que el correo que el usuario ingrese no exista ya  
            //si entro en un error, el contador sumara 1, por lo tanto, no entra a registrar el usuario
            //si entra en los dos, el contador sumara 2, por lo tanto, no entra a registrar el usuario
            //Si no entro a ninguno, el contador seguira valiendo 0, por lo tanto, se procede a registrar el usuario
            if(updateErr==0){
                conn.query('update usuarios set nombre = ?, correoE = ?, contra = ?, rol = ? where idusuario = ?',[nombre,email,contraseña,rol,userid],(err, users)=>{
                    req.flash('success_msg','Datos del usuario \"'+nombre+'\" editados correctamente');
                    res.redirect('/users');   
                });
            }   
            //ponemos el redirect al final, ya que, primero debemos establecer las respuestas y luego enviarselas al cliente
            //(req.flash funciona como una respuesta para el cliente)      
    });
};


controller.deleteUser = (req, res)=>{
    const userid = req.params.idusuario;
    req.getConnection((err, conn)=>{
        conn.query('delete from usuarios where idusuario = ?',[userid],(err, rows)=>{
            req.flash('success_msg','Usuario eliminado correctamente');
            res.redirect('/users');
        })
    })
}
module.exports = controller;

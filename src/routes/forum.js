const express = require('express');
const router = express.Router();

const forumController = require('../controllers/forumController');

router.get('/forum',forumController.listForumNews);

module.exports = router;
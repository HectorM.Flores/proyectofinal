const express = require('express');
const router = express.Router();
const {isAuthenticated} = require('../helpers/auth')

const newsController = require('../controllers/newsController');

router.get('/news',isAuthenticated,newsController.listNews);
router.post('/addnew',isAuthenticated,newsController.saveNews);
router.post('/newssearch',isAuthenticated, newsController.search);
router.get('/deletenew/:idnoticia',isAuthenticated,newsController.deleteNew);
router.get('/updatenew/:idnoticia',isAuthenticated,newsController.editNews);
router.post('/updatenew/:idnoticia',isAuthenticated,newsController.updateNew);

module.exports = router;